from django.forms import ModelForm
from .models import Paciente, Diagnostico, Virus


class PacienteForm(ModelForm):
    class Meta:
        model = Paciente
        fields = ['nome', 'tipo_sang', 'cpf', 'data_nasc', 'genero', 'sintomas', 'data_cad', 'gravidade', 'desc']


class DiagnosticoForm(ModelForm):
    class Meta:
        model = Diagnostico
        fields = ['virus', 'paciente']


class VirusForm(ModelForm):
    class Meta:
        model = Virus
        fields = ['nomeV', 'previnir']
