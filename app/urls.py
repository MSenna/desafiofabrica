from django.urls import path
from .views import CadastroPaciente, CadastroDiagnostico, AtualizarPaciente, DeletarPaciente, Home, CadastroVirus, PacienteList, DiagnosticoList, VirusList

urlpatterns = [
    path('', Home.as_view(), name='Home'),
    path('CadastroVirus/', CadastroVirus.as_view(), name='CadastroVirus'),
    path('CadastroPaciente/', CadastroPaciente.as_view(), name='CadastroPaciente'),
    path('Listar/', PacienteList.as_view(), name='Listar'),
    path('Virus/', VirusList.as_view(), name='Virus'),
    path('CadastroDiagnostico/', CadastroDiagnostico.as_view(), name='CadastroDiagnostico'),
    path('AtualizarPaciente/<int:pk>/', AtualizarPaciente.as_view(), name='AtualizarPaciente'),
    path('DeletarPaciente/<int:pk>/', DeletarPaciente.as_view(), name='DeletarPaciente'),
    path('Diagnostico/', DiagnosticoList.as_view(), name='Diagnostico'),
]