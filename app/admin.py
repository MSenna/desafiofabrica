from django.contrib import admin
from .models import Paciente, Diagnostico, Virus

admin.site.register(Paciente)
admin.site.register(Diagnostico)
admin.site.register(Virus)