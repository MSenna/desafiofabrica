from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView, DeleteView, TemplateView
from .forms import PacienteForm, DiagnosticoForm, VirusForm
from .models import Paciente, Diagnostico, Virus


class CadastroPaciente(CreateView):
    model = Paciente
    form_class = PacienteForm
    template_name = 'CadastroPaciente.html'
    success_url = reverse_lazy('CadastroDiagnostico')


class CadastroDiagnostico(CreateView):
    model = Diagnostico
    form_class = DiagnosticoForm
    template_name = 'CadastroDiagnostico.html'
    success_url = reverse_lazy('Diagnostico')


class DiagnosticoList(ListView):
    model = Diagnostico
    template_name = 'Diagnostico.html'


class PacienteList(ListView):
    model = Paciente
    template_name = "Listar.html"


class VirusList(ListView):
    model = Virus
    template_name = "Virus.html"


class CadastroVirus(CreateView):
    model = Virus
    form_class = VirusForm
    template_name = 'CadastroVirus.html'
    success_url = reverse_lazy('Virus')


class AtualizarPaciente(UpdateView):
    model = Paciente
    form_class = PacienteForm
    template_name = 'AtualizarPaciente.html'
    success_url = reverse_lazy('Listar')


class DeletarPaciente(DeleteView):
    model = Paciente
    form_class = PacienteForm
    template_name = 'DeletarPaciente.html'
    success_url = reverse_lazy('Listar')


class Home(TemplateView):
    template_name = 'Home.html'

