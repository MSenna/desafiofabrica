from django.db import models


class Paciente(models.Model):
    GRAVIDADE = (
        ('leve', 'Leve'),
        ('media', 'Media'),
        ('grave', 'Grave'),
    )

    nome = models.CharField(max_length=30, null=False, blank=False)
    tipo_sang = models.CharField(max_length=3, null=False, blank=False, verbose_name="Tipo Sanguineo")
    cpf = models.CharField(max_length=14, null=False, blank=False, verbose_name="CPF")
    data_nasc = models.DateField(null=False, blank=False, verbose_name="Data de Nascimento")
    genero = models.CharField(max_length=10, null=False, blank=False, verbose_name="Gênero")
    sintomas = models.CharField(max_length=50, null=False, blank=False)
    data_cad = models.DateField(null=False, blank=False, verbose_name="Data do Cadastro")
    gravidade = models.CharField(
        max_length=6,
        choices=GRAVIDADE,
    )
    desc = models.TextField(null=False, blank=False, verbose_name="Descrição")

    def __str__(self):
        return self.nome


class Virus(models.Model):
    nomeV = models.CharField(max_length=15, null=False, blank=False, verbose_name="Virus")
    previnir = models.CharField(max_length=500, null=False, blank=False, verbose_name="Prevenção")

    def __str__(self):
        return self.nomeV


class Diagnostico(models.Model):
    virus = models.ForeignKey(Virus, on_delete=models.SET_NULL, null=True)
    paciente = models.OneToOneField(Paciente, on_delete=models.CASCADE, null=True)
