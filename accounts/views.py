from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('Home')
    else:
        form = UserCreationForm()
    return render(request, 'registration/register.html', {
        'form': form
    })